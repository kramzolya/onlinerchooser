# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 15:11:42 2018

@author: kramzlo
"""

import urllib3
import json
from lxml import html
import csv

urllib3.disable_warnings()
http = urllib3.PoolManager()

def getUrlData(url, product_name, fields, use_total = True):
    print('Starting loading of url data:')    
    print(url)
    
    if fields is None:
        fields = {}
    
    headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Content-Type': 'application/vnd.geo+json; charset=utf-8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Cache-Control': 'no-cache'
     }
    
    fields["page"] = 1
    
    print(fields)
    
    response = http.request('GET', url, fields = fields, headers = headers)
    draft_page = json.loads(response.data)
    
    if 'errors' in draft_page.keys():
        print('error: ' + draft_page)
    print('draft page loaded; keys: ', draft_page.keys())
    print('total items found: ', draft_page["total"])
    print('page: ', draft_page["page"])
    
    total_products = int(draft_page["total"])
    products = draft_page[product_name]
    page_size = draft_page["page"]["limit"]
    pages = draft_page["page"]["last"]

    pages_range = range(2, pages + 1)
    if not use_total:
        pages_range = range(2, total_products//page_size + 30)
    for i in pages_range:
        fields["page"] = i
        print(i, url+'&page=' + str(i))
        response1 = http.request('GET', url, fields = fields, headers = headers)
        draft_page1 = json.loads(response1.data)
        products += draft_page1[product_name]
    return products
    
def elmText(elt):
    return elt.text_content().replace(u'\xa0', u' ').strip().split('\n')[0]

def getAdditionalSpecs(products):
    ### get final specs of selected products (here come additional requests for 100500 pages)---------------------------------------------     
    for i in products:
        page = http.request('GET', i["html_url"])
        html_page = html.fromstring(page.data.decode("utf-8"))
        specs = html_page.xpath('//div[@id = "specs"]')[0]

        table = html_page.xpath('//table[@class = "product-specs__table"]')[0]
        data = [[elmText(td) for td in tr.xpath('td')]  
                for tr in table.xpath('//tr')]    
        data1 = {}
        for row in data:
            if len(row) == 2:
                data1[row[0]] = row[1]
                
        i["specs"] = data1
      
    return products

def apartmentAdditionalSpecs(items):
    c = -1
    for flat in items:
        c += 1
        print(flat["url"], c)
        page = http.request('GET', flat["url"])
        html_page = html.fromstring(page.data.decode("utf-8"))
        #return html_page
        #rent =  html_page.xpath('//div[@class = "arenda-apartment"]')
        
        if len(html_page.xpath("//div[@class = 'special-page']")) > 0:
            flat['actual'] = False
        else:
            flat['actual'] = True
            ##form specs part
            specs_div = html_page.xpath("//div[@class = 'apartment-options']")
            specs_lack_div = html_page.xpath("//div[@class = 'apartment-options__item apartment-options__item_lack']")
            specs = set()
            if len(specs_div) == 1:
                specs = set([i.text for i in specs_div[0]])
            if len(specs_lack_div) > 0:
                specs -= set([i.text for i in specs_lack_div])
            flat["specs"] = specs
            
            ##form conditions part
            conditions_html = html_page.xpath("//div[@class = 'apartment-conditions']")
            conditions = []
            if len(conditions_html) == 1 and len(conditions_html[0]) > 0:
                for i in range(len(conditions_html[0])):
                    conditions.append(conditions_html[0][i].text)
            flat["conditions"] = conditions
            
            info_additional = html_page.xpath('//div[@class = "apartment-info__line"]')
            if len(info_additional[1][0][0]) == 2:
                flat["info_additional"] = info_additional[1][0][0][0].text.strip()
                flat["info_address_additional"] = info_additional[1][0][0][1].text.strip()
    return items

class APARTMENTS_PROPS():
    PRICE = 'price'
    TYPE = 'type'
    OWNER = 'owner'
    ADDRESS = 'address'
    LINK = 'link'
    SPECS = 'specs'
    CREATED = 'created'
    LAST_UPDATE = 'last_update'
    INFO_ADDITIONAL = 'info_additional'
    INFO_ADDRESS_ADDITIONAL = 'info_address_additional'
    
def saveApartmentsShortCSV(name, apartments):
    ### save links to csv file
    with open(name + '.csv', 'w', newline = '') as csvfile:
        fieldnames = [APARTMENTS_PROPS.PRICE, APARTMENTS_PROPS.TYPE, 
                      APARTMENTS_PROPS.CREATED, APARTMENTS_PROPS.LAST_UPDATE, 
                      APARTMENTS_PROPS.OWNER, APARTMENTS_PROPS.ADDRESS, 
                      APARTMENTS_PROPS.LINK, APARTMENTS_PROPS.SPECS]
        writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
        writer.writeheader()
        for flat in apartments:
            writer.writerow({APARTMENTS_PROPS.PRICE: flat['price']['amount'], 
                             APARTMENTS_PROPS.TYPE: flat['rent_type'],
                             APARTMENTS_PROPS.CREATED: flat['created_at'][:16],
                             APARTMENTS_PROPS.LAST_UPDATE: flat['last_time_up'][:16],
                             APARTMENTS_PROPS.OWNER: flat['contact']['owner'],
                             APARTMENTS_PROPS.ADDRESS: flat['location']['address'],
                             APARTMENTS_PROPS.LINK: flat['url'],
                             APARTMENTS_PROPS.SPECS: flat['specs']
                             })


def addSearchFields(fieldsObj, fieldName, objectToAdd):
    if objectToAdd is None:
        return
    for ind, it in enumerate(objectToAdd):
        fieldsObj[fieldName + '[' + str(ind) + ']'] = it
    fieldsObj[fieldName + '[operation]'] = 'union'