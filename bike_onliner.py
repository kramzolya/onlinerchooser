# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 23:38:12 2018

@author: kramzlo
"""
import onliner
import csv
###
bike_classes = None
fork_type = None
bike_material = None
front_transmis = None
####define main constants --------------------------------------------------------------------------------
BRAKE_V = 'V-brake'

#------------------
MOUNTAIN_ALIVIO = 'Alivio'
MOUNTAIN_DEORE = 'Deore'
MOUNTAIN_ACERA = 'Acera'
MOUNTAIN_TOURNEY = 'Tourney'
MOUNTAIN_ALTUS = 'Altus'

BIKE_TYPE_CYCLO = 'cyclocross'
BIKE_TYPE_ROAD = 'road'
BIKE_TYPE_MOUNTAIN = 'mountain'
BIKE_TYPE_HYBRID = 'hybrid'
BIKE_TYPE_GRAVEL = 'gravel'
BIKE_TYPE_TOURING = 'touring'
BIKE_TYPE_SHOSS = 'highway'
BIKE_TYPE_COMFORT = 'comfort'
BIKE_TYPE_CRUISER = 'cruiser'
# also can be add: track, triathlon, tandem, fatbike
FORK_TYPE_HARD = 'hard'
FORK_TYPE_SOFT = 'soft'

TRANSMISSION_PLANETARY = 'planetary'
TRANSMISSION_EXTERNAL = 'external_system'
#also; flip_flop, fixed, singlespeed

MATERIAL_ALUMINIUM = 'aluminum'
MATERIAL_STEEL = 'steel'
MATERIAL_STEEL_853 = 'reynolds853'
MATERIAL_CARBON = 'carbon'
MATERIAL_CHROMOL = 'chrome_molibden'
### define filter params ----------------------------------------------------------------------------------
wheel_from = '28'
wheel_to = '28'
price_from = '500.00'
price_to = '2400.00'
speednumber_from = 5
#bike_classes = [BIKE_TYPE_CYCLO, BIKE_TYPE_ROAD, BIKE_TYPE_HYBRID, BIKE_TYPE_GRAVEL, BIKE_TYPE_TOURING]
#bike_material = [MATERIAL_ALUMINIUM, MATERIAL_STEEL, MATERIAL_STEEL_853, MATERIAL_CARBON, MATERIAL_CHROMOL]
fork_type = [FORK_TYPE_HARD]
front_transmis = [TRANSMISSION_PLANETARY]
### get filtered products (come splitted by pages with 30 producr per page limit) ----------------------
url = 'https://catalog.api.onliner.by/search/bike'
fields = {
        'price[from]': price_from,
        'price[to]': price_to,
        'female': 0,
        'bike_kid_teen': 0,
        'speednumber[from]': speednumber_from,
        'double_rim': 1,
        'group': 1,
        'bwheel_diameter[from]': wheel_from,
        'bwheel_diameter[to]': wheel_to,
        }

onliner.addSearchFields(fields, 'bike_class', bike_classes)
onliner.addSearchFields(fields, 'fork_type', fork_type)
onliner.addSearchFields(fields, 'bike_material', bike_material)
onliner.addSearchFields(fields, 'front_transmis', front_transmis)

bikes = onliner.getUrlData(url, "products", fields)
bikes = onliner.getAdditionalSpecs(bikes)

#split products by groups of shimano equipment-----------------------------------------------------------------------
#-----  ONLY FOR SHIMANO MTB COMPONENTS --------------------------------------------------------------------------------
final_products = {'alivio-deore': {}, 'acera-deore': {}, 'not-tourney' : {}, 'not-tourney-altus' : {}}
final_links = {'alivio-deore': [], 'acera-deore': [], 'not-tourney' : [], 'not-tourney-altus' : []}
final_codes = {'alivio-deore': [], 'acera-deore': [], 'not-tourney' : [], 'not-tourney-altus' : []}

for i in bikes:
    specs_values = i["specs"].values()
    if any([MOUNTAIN_ALIVIO in j or MOUNTAIN_DEORE in j for j in specs_values]) and all(MOUNTAIN_TOURNEY not in j and BRAKE_V not in j and MOUNTAIN_ALTUS not in j for j in specs_values):
        #print(i["html_url"])
        final_products['alivio-deore'].update(i)
        final_links['alivio-deore'].append(i["html_url"])
        final_codes['alivio-deore'].append(i["key"])
    if any([MOUNTAIN_ALIVIO in j or MOUNTAIN_DEORE in j or 'Acera' in j for j in specs_values]) and all(MOUNTAIN_TOURNEY not in j and MOUNTAIN_ALTUS not in j and BRAKE_V not in j for j in specs_values):
        final_products['acera-deore'].update(i)
        final_links['acera-deore'].append(i["html_url"])
        final_codes['acera-deore'].append(i["key"])
    if all(MOUNTAIN_TOURNEY not in j and BRAKE_V not in j for j in specs_values):
        final_products['not-tourney'].update(i)
        final_links['not-tourney'].append(i["html_url"])
        final_codes['not-tourney'].append(i["key"])
    if all(MOUNTAIN_TOURNEY not in j and MOUNTAIN_ALTUS not in j and 'V-brake' not in j for j in specs_values):
        final_products['not-tourney-altus'].update(i)
        final_links['not-tourney-altus'].append(i["html_url"])
        final_codes['not-tourney-altus'].append(i["key"])

### generate compare links
compare_link = {}
const_compare = 'https://catalog.onliner.by/compare/'
compare_link['alivio-deore'] = const_compare + '+'.join(final_codes['alivio-deore'])
compare_link['acera-deore'] = const_compare + '+'.join(final_codes['acera-deore'])
compare_link['not-tourney'] = const_compare + '+'.join(final_codes['not-tourney'])
compare_link['not-tourney-altus'] = const_compare + '+'.join(final_codes['not-tourney-altus'])

### save links to csv file
with open('compare_links.csv', 'w', newline = '') as csvfile:
    fieldnames = ['compare_type', 'link']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for k, v in compare_link.items():
        writer.writerow({'compare_type': k, 'link': v})
###---------------------------------------------------------------------------------------------------------------------