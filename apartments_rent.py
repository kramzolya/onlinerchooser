# -*- coding: utf-8 -*-
"""
Created on Wed Jun 27 16:16:36 2018

@author: kramzlo
"""
import onliner

#### api link
url = "https://ak.api.onliner.by/search/apartments"

#### params
#now set as Minsk and boundaries
fields = {
        'bounds[lb][lat]': 53.74952285419751,
        'bounds[lb][long]': 27.301025390625004,
        'bounds[rt][lat]': 54.04648911335576,
        'bounds[rt][long]': 27.822875976562504,
        '_':0.20683625643704362
        }
#searching options: can be 'room', '1_room', '2_rooms', '3_rooms', etc
#rent_type = ['1_room', '2_rooms']
rent_type = ['2_rooms', '3_rooms', '4_rooms']
price_max = 550

### ready? set? go!
apartments = []
for i in rent_type:
    fields['rent_type[]'] = i
    apartments += onliner.getUrlData(url, "apartments", fields, True)

apartments = onliner.apartmentAdditionalSpecs(apartments)


###############################################################################################
##specs can be: 
#Мебель, Кухонная мебель, Плита, Холодильник, Стиральная машина, Телевизор, Интернет, Лоджия или балкон, Кондиционер
##conditions:
#can be: Не студентам, Без детей, Без животных
class SPECS ():
    FURNITURE = 'Мебель'
    KITCHEN_FURNITURE = 'Кухонная мебель'
    STOVE = 'Плита'
    FRIDGE = 'Холодильник'
    WASHING_MASHINE = 'Стиральная машина'
    TV = 'Телевизор'
    INTERNET = 'Интернет'
    BALCONY = 'Лоджия или балкон'
    AIR_CONDITIONING = 'Кондиционер'

class CONDITIONS():
    STUDENTS = 'Не студентам'
    CHILDREN = 'Без детей'
    ANIMALS = 'Без животных'
        
#############################################################################################
##apply my filters

ap_filtered_3 = []
ap_filtered_2 = []
ap_filtered_m = []

specs_all = set([SPECS.FURNITURE, SPECS.KITCHEN_FURNITURE, SPECS.STOVE, SPECS.FRIDGE, SPECS.WASHING_MASHINE, SPECS.TV, SPECS.INTERNET, SPECS.BALCONY, SPECS.AIR_CONDITIONING])
filter_all = set([SPECS.FRIDGE, SPECS.WASHING_MASHINE, SPECS.FURNITURE])
filter_f_w = set([SPECS.FRIDGE, SPECS.WASHING_MASHINE])
filter_f = set([SPECS.FURNITURE])
for i in apartments:
    if i['actual'] and float(i['price']['amount']) <= price_max and (CONDITIONS.ANIMALS not in i['conditions']):
        lack_items = specs_all - i['specs']
        if filter_all.issubset(lack_items):
            ap_filtered_3.append(i)
        if (not filter_f_w.issubset(lack_items) and not filter_f_w.issubset(i['specs'])) and filter_f.issubset(lack_items):
            ap_filtered_2.append(i)
        if filter_f.issubset(lack_items):
            ap_filtered_m.append(i)


onliner.saveApartmentsShortCSV('no_all_3', ap_filtered_3)
onliner.saveApartmentsShortCSV('furniture_and_fringe_or_washing', ap_filtered_2)
onliner.saveApartmentsShortCSV('only_without_furniture', ap_filtered_m)